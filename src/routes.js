import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import HomeApplication from "./pages/HomeApplication";

class RouteApplication extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path={"/"} component={HomeApplication} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default RouteApplication;
