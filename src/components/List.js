import React, { useState, useEffect, Fragment } from 'react'
import Axios from 'axios'
import Columns from "../pages/intrasit";
import Tabtwo from "../pages/delay";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import Websocket from 'react-websocket';
import intransit from '../pages/intrasit';

export default function List_() {

    const [ data, setData ] = useState(null);
    const [ inTransint, setInTransit ] = useState([]);
    const [ inDelay, setInDelay ] = useState([]);
    const [ timer, setTimer ] = useState(0);

    const parseData = (payload) => {
        const allData = JSON.parse(payload);
        setData(allData);
    }
console.log(parseData);
    useEffect(() => {
        async function loadAPI() {
            const response = await Axios.get('https://bifrost.nextem.com.br/api/calcomp');
    //    const response = await Axios.get('http://3.15.219.93/api/calcomp');
        // const response = await Axios.get('http://localhost:8000/api/calcomp');
 
            setData(response.data);
        }

        loadAPI();
    }, []);

    useEffect(() => {
        choiceData();
    }, [data]);


    function choiceData() { 

        const result = [];

        if (data) {
            data.ALL.map(item => {
                result.push(item);
            });
        }
        // let result = JSON.parse(JSON.stringify(dados));
                    // console.log(result);

        let today = new Date();
        let todayDate = Number(today.getFullYear() + '' + (((today.getMonth() + 1) <= 9) ? "0" + (today.getMonth() + 1) : (today.getMonth() + 1)) + '' + ((today.getDate() <= 9) ? "0" + today.getDate() : today.getDate()));
        // let todayDate = Number(today.getFullYear() + '' + (today.getMonth() + 1) + '' + ((today.getDate() <= 9) ? "0" + today.getDate() : today.getDate()));
        var in_trasint = [];
        var in_delay = [];
        var count = 0;
        var count1 = 0;

       
        for (let i = 0; i < result.length; i++) {
         
            let ata_mao = Number((result[i].ata_mao !== null) ? result[i].ata_mao.replace('-', '').replace('-', '') : 0);
            let ata_mia = Number((result[i].ata_mia !== null) ? result[i].ata_mia.replace('-', '').replace('-', '') : 0);
            let atd_origin = Number((result[i].atd_origin !== null) ? result[i].atd_origin.replace('-', '').replace('-', '') : 0);
            let eta_transbordo = Number((result[i].eta_transbordo!== null) ? result[i].eta_transbordo.replace('-', '').replace('-', '') : 0);
            let ata_transbordo = Number((result[i].ata_transbordo!== null) ? result[i].ata_transbordo.replace('-', '').replace('-', '') : 0);
            let etd_transbordo = Number((result[i].etd_transbordo!== null) ? result[i].etd_transbordo.replace('-', '').replace('-', '') : 0);
            let atd_transbordo = Number((result[i].atd_transbordo!== null) ? result[i].atd_transbordo.replace('-', '').replace('-', '') : 0);
            let etf = Number((result[i].etf !== null) ? result[i].etf.replace('-', '').replace('-', '') : 0);
            let etd_origin = Number((result[i].etd_origin !== null) ? result[i].etd_origin.replace('-', '').replace('-', '') : 0);
            let atd_mia = Number((result[i].atd_mia !== null) ? result[i].atd_mia.replace('-', '').replace('-', '') : 0);
            let eta_mia = Number((result[i].eta_mia !== null) ? result[i].eta_mia.replace('-', '').replace('-', '') : 0);
            let etd_mao =  Number((result[i].etd_mao!== null) ? result[i].etd_mao.replace('-', '').replace('-', '') : 0);
            let etd_mia = Number((result[i].etd_mia !== null) ? result[i].etd_mia.replace('-', '').replace('-', '') : 0);
            let eta_mao =  Number((result[i].eta_mao!== null) ? result[i].eta_mao.replace('-', '').replace('-', '') : 0);
            let ataf = Number((result[i].ataf !== null) ? result[i].ataf.replace('-', '').replace('-', '') : 0);
            let modal = (result[i].modal);
              

                
                // if( (ata_transbordo !== null)  && ( ata_transbordo <= todayDate)){
                //     in_trasint[count] = result[i];
                //    //console.log("in_delay");
                //     count++;
                   
                // }
                // if( (etd_origin !== null)  && ( etd_origin > todayDate)){
                //     in_trasint[count] = result[i];
                //    //console.log("in_delay");
                //     count++;
                   
                // }
                // if( (atd_origin !== null)  && ( atd_origin > todayDate)){
                //     in_trasint[count] = result[i];
                //    //console.log("in_delay");
                //     count++;  
                // }
                // if( (ata_mao !== null)  && ( ata_mao <= todayDate)){
                //     in_trasint[count] = result[i];
                //    //console.log("in_delay");
                //     count++;  
                // }
                // if( (ataf !== null)  && ( ata_mao <= todayDate)){
                //     in_trasint[count] = result[i];
                //    //console.log("in_delay");
                //     count++;  
                // }

        // }


                if (modal === 'SEA') {
 
                    if ( (etd_origin !== 0 && atd_origin <= etd_origin) && (etf === 0 && ataf === 0 && eta_mao === 0 && ata_mao === 0 && ata_transbordo === 0 && eta_transbordo === 0 && etd_transbordo === 0 && atd_transbordo === 0) ) {
                        in_trasint[count] = result[i];
                            count++;
                    }else if((etd_origin !== 0 && atd_origin <= etd_origin) && (eta_transbordo !== 0 && eta_transbordo <= ata_transbordo ) && (etf === 0 && ataf === 0)){
                        in_trasint[count] = result[i];
                            count++;
                    }else if((etd_origin !== 0 && atd_origin <= etd_origin) && (eta_transbordo !== 0 && eta_transbordo <= ata_transbordo && ata_transbordo <= todayDate ) && (etf === 0 && ataf === 0)){
                        in_trasint[count] = result[i];
                            count++;
                    }
                    else if((etd_origin !== 0 && atd_origin <= etd_origin) && (etd_transbordo !== 0 && etd_transbordo <= ata_transbordo && atd_transbordo === 0 ) && (etf === 0 && ataf === 0)){
                        in_trasint[count] = result[i];
                            count++;
                    }
                    else if((etd_origin !== 0 && atd_origin <= etd_origin) && (etd_transbordo !== 0 && etd_transbordo <= ata_transbordo && atd_transbordo <= todayDate ) && (etf === 0 && ataf === 0)){
                        in_trasint[count] = result[i];
                            count++;
                    }
                    else if((etd_origin !== 0 && atd_origin <= etd_origin) && (eta_mao !== 0 &&  ata_mao === 0 && eta_mao <= todayDate  ) && (etf === 0 && ataf === 0)){
                        in_trasint[count] = result[i];
                            count++;
                    }
                    else if((etd_origin !== 0 && atd_origin <= etd_origin) && (eta_mao !== 0 && ata_mao !== 0  && eta_mao <= ata_mao && ata_mao <= todayDate ) && (etf === 0 && ataf === 0)){
                        in_trasint[count] = result[i];
                            count++;
                    }
                    else if(ata_mao !== 0 &&  etf !== 0 && etf <= ataf && ataf !== 0){

                            in_trasint[count] = result[i];
                            count++;
                    }else if(etf !== 0 && ataf !== 0 && etf > ataf){
                         in_delay[count1] = result[i];
                            count1++;   
                    }
                }
                if (modal === 'AIR') {
                    console.log(modal);
                    if ( (etd_origin !== 0 && atd_origin >= etd_origin && ata_mia === 0) && (etf === 0 && ataf === 0 && ata_mao === 0 && atd_mia === 0) ) {
                      
                        in_trasint[count] = result[i];
                            count++;
                    }
                    else if ( (etd_origin !== 0 && atd_origin <= etd_origin) && (ata_mia !== 0 && atd_mia <= todayDate) && (etf === 0 && ataf === 0) ) {
                      
                        in_trasint[count] = result[i];
                            count++;
                    }
                    else if ( (etd_origin !== 0 && atd_origin <= etd_origin) && (etd_mia !== 0 && atd_mia !== 0 && atd_mia >= etd_mia) && (etf === 0 && ataf === 0) ) {
                      
                        in_trasint[count] = result[i];
                            count++;
                    }
                    else if ( (etd_origin !== 0 && atd_origin <= etd_origin) && (eta_mao !== 0 && ata_mao <= eta_mao) && (etf === 0 && ataf === 0) ) {
                      
                        in_trasint[count] = result[i];
                            count++;
                    }
                    else if((etd_origin !== 0 && atd_origin <= etd_origin && eta_mao !== 0 && ata_mao > todayDate) && (etf === 0 && ataf === 0)){
                            in_delay[count1] = result[i];
                            count1++;
                        
                    }
                    // else if ( (etd_origin !== 0 && eta_mao <= todayDate  && etd_mia !== 0) && (atd_mia == null) && (etf === 0 && ataf === 0) ) {
                      
                    //     in_trasint[count] = result[i];
                    //         count++;
                    // }
                    // else if((etd_mia !== 0 && etd_mia < todayDate) && (ata_mao === 0 && etf === 0 && ataf === 0)){
                    //     console.log(etd_mia);
                    //         in_delay[count1] = result[i];
                    //         count1++;
                        
                    // }

                }
                // if (modal === 'AIR/SEA') {

                //     if ( (etd_origin !== 0 && atd_origin <= etd_origin) && (etf === 0 && ataf === 0 && eta_mao === 0 && ata_mao === 0 && etd_mia === 0 && atd_mia === 0 && etd_mia === 0) ) {
                      
                //         in_trasint[count] = result[i];
                //             count++;
                //     }
                //     else if ((eta_mao <= ata_mao) && (etf === 0 && ataf === 0) && (ata_transbordo === 0 && atd_transbordo === 0)) {
                //         in_trasint[count] = result[i];
                //         count++;
                //     }
                //     else if(((ata_mao !== 0 && ata_mao > eta_mao) && (ata_mao < todayDate) && (etf === 0 && ataf === 0 ))){

                //             in_delay[count1] = result[i];
                //             count1++; 
                //     }
                // }

                // if (etd_origin !== 0){  

                //     if ( (atd_origin <= etd_origin) && (etf === 0 && ataf === 0 && ata_mao == 0) ) {
                //          in_trasint[count] = result[i];
                //             count++;
                //     } 
                //     if (condition) {
                        
                //     }

                //         if(((ata_mao !== 0 && ((ata_mao > eta_mao ) && (ata_mao < todayDate))) && (etf == 0 && ataf == 0 ))){

                //             in_delay[count1] = result[i];
                //             count1++;
                //         }else if(((atd_mia != 0 && atd_mia < todayDate) && (ata_mao === 0 && etf === 0 && ataf === 0))){

                //             in_delay[count1] = result[i];
                //             count1++;
                //         }else if(((ata_mao != 0 && ata_mao < todayDate) && (etf < todayDate  && ataf == 0))){

                //             in_delay[count1] = result[i];
                //             count1++;
                        
                //         }else{
                //             in_trasint[count] = result[i];
                //             count++;
                        
                //         }
                // }
            // }
 
            if (result.length) {
               //console.log(in_trasint);
                setInTransit([...inTransint, in_trasint]);
                setInDelay([...inDelay, in_delay]);
                setTimer(result.length*0.5);
                
            }
        }
    }

    return (

        <Fragment>

            <Websocket 
                //  url="ws://bifrost.nextem.com.br:9000/api/calcomp"
                // url="ws://3.15.219.93:9000/api/calcomp"
                url="wss://bifrost.nextem.com.br:9000/api/calcomp"
                    onMessage={parseData}
                    reconnect={true}
                />

        <div className="d-flex flex-column flex-md-row align-items-md-center p-5">
        <div className="wrap p-4">
            <h2>Hermodr <small>  This is the CLC monitor</small></h2>
        {/* <div className="time">Unity: CR <span id="display"></span></div> */}
    
            <table>  
            {/* Estrutura base das colunas do monitor    */}
            <tbody>
            <tr>
                <th width="18%">CLC</th>
                <th width="16%">PROJECTS</th>
                <th width="9%">ETD</th>
                <th width="9%">ETA</th>
                <th width="9%">ATA</th>
                <th width="9%">ETAF</th>
                <th width="11%">MODAL</th>
                <th width="11%">CHANNEL</th>                  
            </tr>
            </tbody>
            </table>

                <div className="conteudo">
                    <Router>
                        <Switch>

                                <Route path={"/all"}> 
                                    <table id="table"  style={{ animation:"marquee "+timer+"s linear infinite"}}>
                                        <h4>In Transit</h4>
                                        <Columns information={inTransint} className="teste" unit={'ALL'}/>
                                        <h4>In delay</h4>
                                        <Tabtwo information={inDelay} unit={'ALL'}/> 
                                        {/* <Tabtwo information={this.state.data.inDelay}/>  */}
                                    </table> 
                                    
                                </Route>  
                                <Route path={"/cs"}> 
                                    <table id="table"  style={{ animation:"marquee "+timer+"s linear infinite"}}>
                                        <h4>In Transit</h4>
                                        <Columns information={inTransint} className="teste" unit={'CS'}/>
                                        <h4>In delay</h4>
                                        <Tabtwo information={inDelay} unit={'CS'}/> 
                                        {/* <Tabtwo information={this.state.data.inDelay}/>  */}
                                    </table> 
                                    
                                </Route>
                                <Route path={"/cr"}> 
                                    <table id="table"  style={{ animation:"marquee "+timer+"s linear infinite"}}>
                                        <h4>In Transit</h4>
                                        <Columns information={inTransint} className="teste" unit={"CR"}/>
                                        <h4>In delay</h4>
                                        <Tabtwo information={inDelay} unit={"CR"}/> 
                                        {/* <Tabtwo information={this.state.data.inDelay}/>  */}
                                    </table>  
                                </Route>     
                                                      
                        </Switch>
                 
                    </Router>
                </div>
        </div>
    </div>
    </Fragment>
    )
}
