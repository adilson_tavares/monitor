import React, { Component } from "react";
import { Card, Header, ListContent, ItemCLC, NoUpdate, TitleCLC } from "./style";

export default class CardComponent extends Component {
  render() {
    return (
      <Card>
        <Header>{this.props.title}</Header>
        <ListContent>
          {this.props.informations.length ? (
            this.props.informations.map((clc, index) => {
              //console.log(clc);
              return (
                <ItemCLC status={this.props.status} key={index}>
                  <TitleCLC>{clc.code}</TitleCLC>
                  <br />
                  Invoices: {clc.invList}
                </ItemCLC>
              );
            })
          ) : (
            <NoUpdate>No updates to display</NoUpdate>
          )}
          <footer>{this.props.informations.length} CLCS</footer>
        </ListContent>
      </Card>
    );
  }
}
