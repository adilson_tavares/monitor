import styled from "styled-components";

const Card = styled.div`
  width: 32.80%;
  height: calc(100% - 10px - 17px);
  margin-top: 20px;

  > * {
    background-color: #f3f3f3;
    color: #333;
    padding: 0 10px;
  }

  header {
    line-height: 36px;
    font-size: 16px;
    font-weight: bold;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
  }

  footer {
    line-height: 36px;
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
    color: #888;
  }
`;

const Header = styled.div`
  line-height: 36px;
  font-size: 16px;
  font-weight: bold;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  text-transform: uppercase;
`;

const ListContent = styled.ul`
  list-style: none;
  margin: 0;
  max-height: calc(100% - 36px - 36px);
  overflow-y: auto;
`;

const ItemCLC = styled.div`
  background-color: ${props =>
    props.status === "releases"
      ? "#18b372"
      : props.status === "transits"
      ? "#ffffc8"
      : props.status === "arrears"
      ? "#ff7272"
      : "#fff"};
  color: ${props =>
    props.status === "releases"
      ? "#fff"
      : props.status === "transits"
      ? "black"
      : props.status === "arrears"
      ? "#fff"
      : "black"};
  padding: 20px 10px;
  font-size: 1.1rem;
  line-height: 1.1rem;

  &:not(:last-child) {
    margin-bottom: 10px;
  }

  border-radius: 3px;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);

  img {
    display: block;
    width: calc(100% + 2 * 10px);
    margin: -10px 0 10px (-10px);
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
  }
`;

const TitleCLC = styled.div`
  font-weight: bold;
  font-size: 2.1rem;
  line-height: 2.0rem;
`;


const NoUpdate = styled.div`
  text-align: center;
  padding: 6% 0;
  font-weight: bold;
  font-size: 1.5rem;
  color: #656565;
`;

export { Card, Header, ListContent, ItemCLC, NoUpdate, TitleCLC };
