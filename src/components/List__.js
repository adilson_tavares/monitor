import React, { Fragment } from 'react'
import Axios from 'axios'
import Columns from "../pages/intrasit";
import Tabtwo from "../pages/delay";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";


export default class  List extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
        }
        // this.state = {
        //     all: [],
        //     cr:[],
        //     cs: [],
        //     data: {
        //         teste: [],
        //         inTrasint: [],
        //         inDelay: [],
        //         items: [],
        //     },
        //     route: "clc1",
        //     currentCount: 0,
        //     intervalId: '',
        //     timer: 0,
        // }
    }
   
    componentDidMount(){
        Axios.get('https://bifrost.nextem.com.br/api/calcomp').then(res => {

            // console.log(res.data['ALL']);
        //     all:res.data['ALL'], 
        //    cr:res.data['CR'], 
        //    cs:res.data['CS'],
            
        this.setState({ 
            data: res.data,
        });

    });
    
    console.log(this.state.data);
    
       
    }
 
    componentWillUnmount() {
       
        clearInterval(this.interval);
    }

    choiceData(dados){
       let result = JSON.parse(JSON.stringify(dados));
       console.log(this.state.all);
       let today = new Date();
       let todayDate = Number(today.getFullYear() + '' + (today.getMonth() + 1) + '' + ((today.getDate() <= 9) ? "0" + today.getDate() : today.getDate()));
       var in_trasint = [];
       var in_delay = [];
       var count = 0;
       var count1 = 0;
        var result1 = result['ALL'];
        console.log(result1);

       for (let i = 0; i < result.length; i++) {
        
           let ata_mao = Number((result['ALL'][i].ata_mao !== null) ? result['ALL'][i].ata_mao.replace('-', '').replace('-', '') : 0);
           let etf = Number((result['ALL'][i].etf !== null) ? result['ALL'][i].etf.replace('-', '').replace('-', '') : 0);
           let etd = Number((result['ALL'][i].etd_origin !== null) ? result['ALL'][i].etd_origin.replace('-', '').replace('-', '') : 0);
           let eta =  Number((result['ALL'][i].eta_mao!== null) ? result['ALL'][i].eta_mao.replace('-', '').replace('-', '') : 0);
           let ataf = Number((result['ALL'][i].ataf !== null) ? result['ALL'][i].ataf.replace('-', '').replace('-', '') : 0);
           console.log(ata_mao);
        if (etd !== 0){
            console.log(etd);
            if(ataf !== 0){
                in_trasint[count] = result['ALL'][i];
                count++;
            }
            else{ 
                if((ata_mao !== 0 && ata_mao < todayDate) && etf < todayDate ){
                    in_delay[count1] = result['ALL'][i];
                    count1++;
                }else if(eta !== 0 && eta < todayDate && ata_mao === 0 && etf === 0){
                    in_delay[count1] = result['ALL'][i];
                    count1++;
                }else if(ata_mao !== 0 && ata_mao < todayDate && etf < todayDate){
                    in_delay[count1] = result['ALL'][i];
                    count1++;
                }else{
                    in_trasint[count] = result['ALL'][i];
                    count++;
                }
            }
        }

        if (result['ALL'].length) {
           // console.log(result);
            
           this.setState({
            data: {
                inTrasint: in_trasint,
                inDelay: in_delay,
                teste: in_trasint,
            },
            timer: result['ALL'].length*2,
        });
        }
           
       }

      
       
    }

    timer(){
        var newCount = this.state.currentCount - 1;
        // console.log(newCount);
        var flag = this.state.route;
        

        if(newCount >= 0) { 
            this.setState({ currentCount: newCount });
        } else {
            this.setState({ currentCount: 10});
            if(flag === "clc1")
            {
                this.setState({
                    route: "clc2",
                })
            }
            else if(flag === "clc2")
            {
                this.setState({
                    route: "clc1",
                })
            }
        }
    }

    timerScroll()
    {
        let timerScroll = (this.state.data.inTrasint.length * 4) + (this.state.data.inDelay.length*4);
        return timerScroll;
    }

    route(rota){       
        return rota;
    };

    render() {
       console.log(this.state.data.inTrasint);
       
        return (
            
            <Fragment>
                <div className="d-flex flex-column flex-md-row align-items-md-center p-5">
                <div className="wrap p-4">
                    <h2>Hermodr <small>  This is the CLC monitor</small></h2>
                {/* <div className="time">Unity: CR <span id="display"></span></div> */}
            
                    <table>  
                    {/* Estrutura base das colunas do monitor    */}
                    <tbody>
                    <tr>
                        <th width="18%">CLC</th>
                        <th width="16%">PROJECTS</th>
                        <th width="9%">ETD</th>
                        <th width="9%">ETA</th>
                        <th width="9%">ATA</th>
                        <th width="9%">ETAF</th>
                        <th width="11%">MODAL</th>
                        <th width="11%">CHANNEL</th>                  
                    </tr>
                    </tbody>
                    </table>

 
                        <div className="conteudo">
                            <Router>
                                <Switch>

                                    <Route path={"/all"}> 
                                        <table id="table"  style={{ animation:"marquee "+this.state.data.timer+"s linear infinite"}}>
                                            {/* <h4>In Transit</h4> */}
                                            <Columns information={this.state.data.inTrasint} className="teste" unit={'ALL'}/>
                                            {/* <h4>In delay</h4> */}
                                            <Tabtwo information={this.state.data.inDelay} unit={'ALL'}/> 
                                             {/* <Tabtwo information={this.state.data.inDelay}/>  */}
                                        </table> 
                                        
                                    </Route>  
                                    <Route path={"/cs"}> 
                                    <table id="table"  style={{ animation:"marquee "+this.state.data.timer+"s linear infinite"}}>
                                        <h4>In Transit</h4>
                                        <Columns information={this.state.data.inTrasint} className="teste" unit={'CS'}/>
                                        <h4>In delay</h4>
                                        <Tabtwo information={this.state.data.inDelay} unit={'CS'}/> 
                                        {/* <Tabtwo information={this.state.data.inDelay}/>  */}
                                    </table> 
                                    
                                </Route>
                                <Route path={"/cr"}> 
                                    <table id="table"  style={{ animation:"marquee "+this.state.data.timer+"s linear infinite"}}>
                                        <h4>In Transit</h4>
                                        <Columns information={this.state.data.inTrasint} className="teste" unit={"CR"}/>
                                        <h4>In delay</h4>
                                        <Tabtwo information={this.state.data.inDelay} unit={"CR"}/> 
                                        {/* <Tabtwo information={this.state.data.inDelay}/>  */}
                                    </table> 
                                    
                                </Route>                                                                 
                                </Switch>
                         
                            </Router>
                        </div>
          
                </div>
            </div>
            </Fragment>

        );
    }
}