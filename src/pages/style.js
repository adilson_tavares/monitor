import styled from "styled-components";

const Lists = styled.div`
  display: flex;
  overflow-x: auto;
  > * {
    flex: 0 0 auto;
    margin-left: 10px;
  }
  &::after {
    content: "";
    flex: 0 0 10px;
  }
`;

const HeaderApplication = styled.div`
  padding: 15px 10px;
  align-items: center;
  background-color: #18b372;
  font-size: 1.1rem;
  color: #fff;
  text-align: center;
  font-weight: bold;
  text-transform: uppercase;
  -webkit-app-region: drag;
`;

const UpdateButton = styled.button`
  position: absolute;
  right: 20px;
  top: 12px;
  background: #5c3fff;
  border: 0;
  color: #fff;
  padding: 5px 10px;
  -webkit-app-region: no-drag;
`;

export { Lists, HeaderApplication, UpdateButton };
