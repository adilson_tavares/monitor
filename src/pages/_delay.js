import React, { Component} from "react";
import air from '../image/plane-ship.svg';
import sea from '../image/sea-ship.svg';
import airsea from '../image/plane-ship.svg';
import green from '../image/green.svg';
import red from '../image/red.svg';
import Moment from 'react-moment';
import 'moment-timezone';

export default class Tabtwo extends Component{
    
    handleNew = (etd) => {
        // Funçao que retorna as CLCs New do monitor
        const today = new Date();
        const todayDate = Number(today.getFullYear() + '' + (today.getMonth() + 1) + '' + ((today.getDate() <= 9) ? "0" + today.getDate() : today.getDate()))
        const tomorrow = todayDate + 1;
        const yesterday = todayDate -1;
        const etd_mao = Number((etd !== null) ? etd.replace('-', '').replace('-', '') : 0);
        
        if(etd_mao >= yesterday && etd_mao < tomorrow) { 
            return "badge badge-new";
        }else{
            return "NaN";
        }
    }

    render() {

        return (
            // Print CLCs
            <>
            {this.props.information ? (
               this.props.information.map((clc, index)=> {
                if(clc.unit === this.props.unit || this.props.unit === "ALL"){
                   return(
                       <tr className="bg-delay">
                        {clc.ataf !== null ? (
                                <td width="18%" className="clc-code">{clc.cod_clc} <span class={clc.ataf != null ? "badge badge-finished" : ""}>{clc.ataf != null ? "Finished" : ""}</span></td>
                            ) : (
                                <td width="18%" className="clc-code"> {clc.cod_clc} <span className={this.handleNew(clc.etd_origin)}>{this.handleNew(clc.etd_origin) !== "NaN" ? "new" : ""}</span> </td>
                            )
                        }
                       <td width="16%">{clc.project} </td>
                       <td width="9%">
                       {clc.etd_origin ? (
                       <Moment  format="MMM, Do">
                           {clc.etd_origin}
                       </Moment>
                           
                           ) : (
                           "--"
                       )}
                       </td>
                       <td width="9%">
                       {clc.eta_mao ? (
                       <Moment  format="MMM, Do">
                           {clc.eta_mao}
                       </Moment>
                           
                           ) : (
                           "--"
                       )}
                       </td>
                       <td width="9%">
                       {clc.ata_mao ? (
                       <Moment  format="MMM, Do">
                           {clc.ata_mao}
                       </Moment>
                           
                           ) : (
                           "--"
                       )}
                       </td>
                       <td width="9%">
                           {clc.etf ? (
                           <Moment  format="MMM, Do">
                               {clc.etf}
                            </Moment>
                            ) : ( 
                            "--"
                            )}
                       </td>
                       <td width="11%" className="column-modal">{clc.modal === 'AIR' ? (<img src={air} alt="" height="24"/>):clc.modal === 'SEA' ? (<img src={sea} alt="" height="24"/>):clc.modal === 'AIR/SEA' ? (<img src={airsea} alt="" height="24"/>):"--"} {clc.modal}</td>
                           <td width="11%">{clc.siscomex_channel === 'Green' ? (<img src={green} alt="" height="18"/>):clc.siscomex_channel === 'Red' ? (<img src={red} alt="" height="18"/>):"--"} {clc.siscomex_channel}</td>
                       </tr>
                   );
                    }
                   })
            ): (
               <tr><td height="100px">No updates to display</td></tr>
             )}
            </>
           );
    }

}